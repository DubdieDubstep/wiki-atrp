---
title: "Default Groups"
---

These groups are automatically created by EssentialMode, please note that superadmin is the highest. Creating a "owner" role that derives from this is just dumb, just give yourself "superadmin". As superadmin is the only group that is hard-coded to overwrite **any** permission check (So be careful who you give it to).

```lua
-- User group, lowest possible.
user = Group("user", "")
 
-- Admin, is allowed to run administrative functions
admin = Group("admin", "user")
 
-- Superadmin, can run any command, bypasses command levels
superadmin = Group("superadmin", "admin")
```