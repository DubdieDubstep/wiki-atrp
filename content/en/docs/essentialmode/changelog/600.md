---
title: "6.0.0"
weight: 900
---


12 Jan 2019 
### Changelog

    Periodic update checking
    Removed UI component from base EssentialMode
    SQLite as default database now
    Now licensed under AGPLv3
    Allow disabling of EssentialModes command handler
    Much better ACL support
    Disabled developer tools by default

### Important note

Follow the installation instructions at https://docs.kanersps.pw/docs/essentialmode/installation so you don’t miss anything. As for updating follow all of that aswell.
SQLite

SQLite is a single file database which doesn’t require any additional setup, if you would like to use this all you have to do is drag and drop EssentialMode into your resources folder and start it. You don’t have to start any external database service at all.
### Events

No changes

**Compatibility issues**

The default database isn't CouchDB anymore. It will now be SQLite to revert back please use the convar es_defaultDatabase


