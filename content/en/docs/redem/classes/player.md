---
title: "Player"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Structure
```lua
{
    self.source = source
	self.permission_level = permission_level
	self.money = money
	self.bank = bank
	self.identifier = identifier
	self.license = license
	self.group = group
	self.session = {}
	self.roles = roles
}
```

### Functions
Here we are assuming that the "Player" class is retrieved with [redem:getPlayerFromId](/redem/events/#retrieve-player) so that we have access to "user"
```lua
    user.kick(reason)
    user.setSessionVar(key, value)
    user.getSessionVar(key)
    user.getPermissions()
    user.setPermissions(level)
    user.getIdentifier()
    user.getGroup()
    user.setGlobal(key, default)
    user.hasRole(role)
    user.giveRole(role)
    user.removeRole(role)
    user.set(key, value)
    user.get(key)
```

### Details
#### user.setSessionVar
This function allows you to set session variables on a user, like the name suggests, these variables only last the amount of time they are in the server. There is one pre-set session variable which may prove useful to you


```lua
-- Returns the current active character id for a user
user.getSessionVar("charid")
```