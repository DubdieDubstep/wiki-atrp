---
title: "Notifications"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Introduction
RedEM:RP Offers a library to create notifications, to use this library you can utilize the below functions and events

### Functions
Exports you can utilize on the client to show notifications

```lua
-- Where the values are (0, first text, time)
exports.redem_roleplay.DisplayTip(0, "your text", 8000)

-- Where the values are (0, first text, time)
exports.redem_roleplay.DisplayTopCenterNotification(0, "your text", 8000)

-- Where the values are (0, first text, second text, texture dict, icon, time)
exports.redem_roleplay.DisplayLeftNotification(0, "your text", "second text", "generic_textures", "tick", 8000)
```

### Events
You can also choose to use events if you'd prefer

```lua
TriggerClientEvent('redem_roleplay:NotifyLeft', "first text", "second text", "generic_textures", "tick", 8000)
TriggerClientEvent('redem_roleplay:Tip', "your text", 8000)
TriggerClientEvent('redem_roleplay:NotifyTop', "your text", 8000)
```