---
title: "Dialog"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Introduction
Created by amakuu to provide a "Fallout 4" style dialog interaction menu. Can be found at [https://github.com/RedEM-RP/redemrp_dialog](https://github.com/RedEM-RP/redemrp_dialog)

### Events
To create a dialog, you have to first call an event

```lua
-- Serverside
TriggerEvent("redemrp_dialog:createDialog", ID, "TITLE", "SUBTITLE/CONTEXT",
    {
        {name = "OPTION 1", func = function(source) TriggerEvent('redemrp_dialog:openDialog', ID , source) end}, -- OPENS NEXT DIALOG
        {name = "OPTION 2", func = function() print("SELECTED 2") end}, -- PRINTING 2 IN CONSOLE
        {name = "OPTION 3", func = function() end, focusOFF = true} -- CLOSES AND UNFOCUSING MENU
    }
)
```

Then if you'd like to open a dialog, you can do the following
```lua
-- Clientside
TriggerServerEvent("redemrp_dialog:openDialog", ID)
```